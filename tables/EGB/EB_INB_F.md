# EB_INB_F

référentiel des bénéficiaires de l'EGB


## Modèle de données

|Nom|Type|Description|Exemple|Propriétés|
|-|-|-|-|-|
|BEN_QLT|chaîne de caractères|Qualité du bénéficiaire (A=assuré AD=ayant-droit)|||
|ORG_AFF_DTF|date|Date de fin de rattachement à l'ancien organisme|||
|BEN_SEX_COD|nombre réel|Code sexe du bénéficiaire|||
|BEN_RES_DPT|chaîne de caractères|Département de résidence du bénéficiaire|||
|BEN_NIR_IDT|chaîne de caractères|NIR anonyme du bénéficiaire dans l'échantillon|||
|BEN_DCD_AME|chaîne de caractères|Année et mois de décès du bénéficiaire|||
|MAJ_SYS_DTE|date|Date de mise à jour des données du bénéficiaire dans le référentiel|||
|UPD_DTE|date|Date de la dernière sélection des bénéficiaires de l'EGB|||
|BEN_NAI_ANN|chaîne de caractères|Année de naissance du bénéficiaire|||
|BEN_DCD_DTD|date|Date de décès du bénéficiaire dans le référentiel de l'EGB|||
|BEN_MVT_TOP|chaîne de caractères|Code des mouvements du bénéficiaire|||
|SLM_RTT_COD|chaîne de caractères|ancienne SLM de rattachement au RNIAM|||
|GRG_RTT_COD|chaîne de caractères|Grand régime de rattachement au RNIAM du bénéficiaire|||
|INS_DTE|date|Date d'insertion|||
|CAI_RTT_COD|chaîne de caractères|caisse de rattachement au RNIAM|||
|BEN_NAI_ANM|chaîne de caractères|Année et mois de naissance du bénéficiaire|||
